# Angular Crash Course 2021 (Task Tracker App)

It includes the Angular ui 11 and JSON-server for our mock backend

## Usage

### Install dependencies

```
npm install
```

### Run Angular server (http://localhost:4200)

```
ng serve
```

### Run the JSON server (http://localhost:5000)

```
npm run server
```

### To build for production

```
ng build
```
